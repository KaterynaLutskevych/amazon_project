package Tests;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchAndFiltersTests extends BaseTest {

    private String KEYWORD_TO_SEARCH = "laptop";
    private String FILTER_RESULT = "HP";
    private int NUMBER_OF_ITEMS_ON_THE_PAGE=24;
    private String NAME_OF_OPERATING_SYSTEM="Windows";


    @Test(priority = 1)
    public void searchByKeyWord() {
        getHomePage().searchByKeyWord(KEYWORD_TO_SEARCH);
        Assert.assertTrue(gerDriver().getCurrentUrl().contains(KEYWORD_TO_SEARCH));
    }

    @Test(priority = 2)
    public void checkFilterByTradeName() {
        getHomePage().searchByKeyWord(KEYWORD_TO_SEARCH);
        getBasePage().waitForElementVisibility(30, getLaptopPage().getCheckboxFilterHP());
        getLaptopPage().clickCheckboxFilterHP();
        for (WebElement webElement : getLaptopPage().getLaptopsFilteredList()) {
            Assert.assertTrue(webElement.getText().contains(FILTER_RESULT));
        }
    }

    @Test (priority = 3)
    public void isOperatingSystemSelected(){
        getHomePage().searchByKeyWord(KEYWORD_TO_SEARCH);
        getBasePage().waitForElementVisibility(30, getLaptopPage().getOperatingSystem());
        getLaptopPage().clickOperatingSystem();
        getBasePage().waitForElementVisibility(45, getLaptopPage().getMenuOfSelectedFilters());
        Assert.assertTrue(getLaptopPage().getMenuOfSelectedFilters().getText().contains(NAME_OF_OPERATING_SYSTEM));
    }

    @Test (priority = 4)
    public void isTotalNumberOfItemsOnPageIsCorrect(){
        getHomePage().searchByKeyWord(KEYWORD_TO_SEARCH);
        getBasePage().waitForElementVisibility(45, getLaptopPage().getCheckboxFilterHP());
        getLaptopPage().clickCheckboxFilterHP();
        getBasePage().waitForPageReadyState(45);
        Assert.assertEquals(getLaptopPage().getLaptopsFilteredList().size(),NUMBER_OF_ITEMS_ON_THE_PAGE);
    }

}
