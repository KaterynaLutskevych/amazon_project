package Tests;


import Pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

public class BaseTest {

    private WebDriver driver;
    private static final String AMAZON_URL="https://www.amazon.com/";

    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(AMAZON_URL);
        getBasePage().implicitlyWait(30);
    }

    @AfterMethod
    public void tearDown () { driver.close(); }

    public WebDriver gerDriver(){ return driver; }
    public HomePage getHomePage(){ return new HomePage(driver); }
    public BasePage getBasePage(){ return new BasePage(driver); }
    public LaptopPage getLaptopPage(){return new LaptopPage(driver);}
    public HpLaptopPage getHpLaptopPage(){return new HpLaptopPage(driver);}
    public CartPage getCartPage(){return new CartPage(driver);}
    public SignInPage getSignInPage(){return  new SignInPage(driver);}
    public RegistrationPage getRegistrationPage(){return new RegistrationPage(driver);}
}
