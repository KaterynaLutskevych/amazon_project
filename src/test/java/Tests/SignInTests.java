package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SignInTests extends BaseTest{

    private String INVALID_DATA_FOR_SIGN_IN="12345";

    @Test(priority = 1)
    public void signInEnteringInvalidData(){
        getHomePage().clickSignInButton();
        getBasePage().implicitlyWait(30);
        getSignInPage().enterDataIntoSignInForm(INVALID_DATA_FOR_SIGN_IN);
        getBasePage().implicitlyWait(30);
        Assert.assertTrue(getSignInPage().getPopupWithWarning().isDisplayed());
    }

    @Test (priority = 2)
    public void checkNavigationToRegistrationForm(){
        getHomePage().clickSignInButton();
        getBasePage().implicitlyWait(30);
        getSignInPage().clickCreateNewAccountButton();
        getBasePage().implicitlyWait(30);
        Assert.assertTrue(getRegistrationPage().getRegistrationForm().isDisplayed());
    }

    @Test (priority = 3)
    public void registrationWithEmptyFieldsLeft(){
        getHomePage().clickSignInButton();
        getBasePage().implicitlyWait(30);
        getSignInPage().clickCreateNewAccountButton();
        getBasePage().implicitlyWait(30);
        getRegistrationPage().clickCreateAccountButton();
        Assert.assertTrue(getRegistrationPage().getWarningMissedName().isDisplayed());
        Assert.assertTrue(getRegistrationPage().getWarningMissedEmail().isDisplayed());
        Assert.assertTrue(getRegistrationPage().getWarningMissedPassword().isDisplayed());
    }
}
