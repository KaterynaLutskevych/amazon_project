package Tests;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

public class CartTests extends BaseTest{

    private String KEYWORD_TO_SEARCH = "laptop";
    private String ITEMS_IN_CART = "1";
    private String EMPTY_CART = "0";

    @Test(priority = 1)
    public void addItemToCart() {
        getHomePage().searchByKeyWord(KEYWORD_TO_SEARCH);
        getBasePage().waitForElementVisibility(30, getLaptopPage().getCheckboxFilterHP());
        getLaptopPage().clickCheckboxFilterHP();
        getBasePage().waitForElementVisibility(45, getLaptopPage().getLaptopHpToBuy());
        getLaptopPage().clickLaptopHpToBuy();
        getBasePage().waitForElementVisibility(30, getHpLaptopPage().getAddToCartButton());
        getHpLaptopPage().clickAddToCartButton();
        getHpLaptopPage().clickLogo();
        assertEquals(getHomePage().getQuantityOfItemsInCart(), ITEMS_IN_CART);
    }

    @Test(priority = 2)
    public void deleteItemFromCart() {
        getHomePage().searchByKeyWord(KEYWORD_TO_SEARCH);
        getBasePage().waitForElementVisibility(30, getLaptopPage().getCheckboxFilterHP());
        getLaptopPage().clickCheckboxFilterHP();
        getBasePage().waitForElementVisibility(45, getLaptopPage().getLaptopHpToBuy());
        getLaptopPage().clickLaptopHpToBuy();
        getBasePage().waitForElementVisibility(30, getHpLaptopPage().getAddToCartButton());
        getHpLaptopPage().clickAddToCartButton();
        getHpLaptopPage().clickLogo();
        getHomePage().clickButtonCart();
        getBasePage().waitForElementVisibility(30, getCartPage().getDeleteFromCart());
        getCartPage().clickDeleteFromCart();
        assertEquals(getHomePage().getQuantityOfItemsInCart(), EMPTY_CART);
    }

    @Test(priority = 3)
    public void checkThePriceBeforeAddToCartAndAfter(){
        getHomePage().searchByKeyWord(KEYWORD_TO_SEARCH);
        getBasePage().waitForElementVisibility(30, getLaptopPage().getCheckboxFilterHP());
        getLaptopPage().clickCheckboxFilterHP();
        getBasePage().waitForElementVisibility(30, getLaptopPage().getLaptopHpToBuy());
        getLaptopPage().clickLaptopHpToBuy();
        getBasePage().waitForElementVisibility(30, getHpLaptopPage().getAddToCartButton());
        getHpLaptopPage().clickAddToCartButton();
        assertEquals(getHpLaptopPage().getPriceOfItemBeforeAddedToCart(),getHpLaptopPage().getPriceOfItemAfterAddedToCart());
    }
}
