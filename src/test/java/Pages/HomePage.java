package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath="//input[@id='twotabsearchtextbox']")
    private WebElement searchInput;

    @FindBy(xpath="//span[@id='nav-cart-count']")
    private WebElement quantityOfItemsInCart;

    @FindBy(xpath = "//a[contains(@href,'nav_cart')]")
    private WebElement buttonCart;

    @FindBy(xpath="//div[@id='nav-tools']//a[contains(@href,'signin')]")
    private WebElement signInButton;

    public HomePage(WebDriver driver){
        super(driver);
    }

    public void searchByKeyWord(String keyword){
        searchInput.sendKeys(keyword,Keys.ENTER);
    }
    public String getQuantityOfItemsInCart(){
        return quantityOfItemsInCart.getText();
    }
    public void clickButtonCart(){
        buttonCart.click();
    }
    public WebElement getButtonCart(){
        return buttonCart;
    }
    public void clickSignInButton(){
        signInButton.click();
    }

}
