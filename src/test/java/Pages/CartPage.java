package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage{

    @FindBy(xpath="//input[contains(@aria-label,'Delete HP')]")
    private WebElement deleteFromCart;

    public CartPage(WebDriver driver){
        super(driver);
    }

    public void clickDeleteFromCart(){
        deleteFromCart.click();
    }
    public WebElement getDeleteFromCart(){
        return deleteFromCart;
    }
}
