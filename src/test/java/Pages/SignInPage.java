package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage{

    @FindBy(xpath="//input[@type='email']")
    private WebElement fieldToEnterDataToSignIn;

    @FindBy (xpath="//div[@id='auth-error-message-box']/div[@class='a-box-inner a-alert-container']")
    private WebElement popupWithWarning;

    @FindBy (xpath="//a[@id='createAccountSubmit']")
    private WebElement createNewAccountButton;

    public SignInPage(WebDriver driver){
        super(driver);
    }

    public void enterDataIntoSignInForm(String keyword){
        fieldToEnterDataToSignIn.sendKeys(keyword, Keys.ENTER);
    }
    public WebElement getPopupWithWarning(){
        return popupWithWarning;
    }

    public void clickCreateNewAccountButton(){
        createNewAccountButton.click();
    }
}
