package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LaptopPage extends BasePage{

    @FindBy(xpath="//li[@id='p_89/HP']//i[@class='a-icon a-icon-checkbox']")
    private WebElement checkboxFilterHP;

    @FindBy(xpath="//a[contains(@class,'a-link-normal')]/span[contains(@class,'a-text-normal')]")
    private List<WebElement> laptopsFilteredList;

    @FindBy(xpath="//a[contains(@href,'HP-15-Inch-i5')]/span[contains(@class,'a-text-normal')]")
    private WebElement laptopHpToBuy;

    @FindBy(xpath="//a[contains(@href,'operating_system')]/span[contains(text(),'Windows')]")
    private WebElement operatingSystem;

    @FindBy(xpath="//span[@cel_widget_id='UPPER-RESULT_INFO_BAR']//div[contains(@class,'sg-col-14')]/div[@class='sg-col-inner']")
    private WebElement menuOfSelectedFilters;

    public LaptopPage(WebDriver driver){
        super(driver);
    }

    public void clickCheckboxFilterHP(){
        checkboxFilterHP.click();
    }
    public WebElement getCheckboxFilterHP() {
        return checkboxFilterHP;
    }
    public List<WebElement> getLaptopsFilteredList(){
        return laptopsFilteredList;
    }
    public void clickLaptopHpToBuy(){
        laptopHpToBuy.click();
    }
    public WebElement getLaptopHpToBuy(){
        return laptopHpToBuy;
    }
    public void clickOperatingSystem(){ operatingSystem.click(); }
    public WebElement getOperatingSystem(){
        return operatingSystem;
    }
    public WebElement getMenuOfSelectedFilters(){
        return menuOfSelectedFilters;
    }

}
