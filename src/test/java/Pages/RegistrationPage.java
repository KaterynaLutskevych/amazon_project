package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends BasePage{

    @FindBy(xpath="//div[@class='a-box-inner']")
    private WebElement registrationForm;

    @FindBy(xpath="//input[@id='continue']")
    private WebElement createAccountButton;

    @FindBy(xpath="//div[@id='auth-email-missing-alert']//div[contains(text(),'Enter your email')]")
    private WebElement warningMissedEmail;

    @FindBy(xpath="//div[@id='auth-customerName-missing-alert']//div[contains(text(),'Enter your name')]")
    private WebElement warningMissedName;

    @FindBy(xpath="//div[@id='auth-password-missing-alert']//div[contains(text(),'Enter your password')]")
    private WebElement warningMissedPassword;


    public RegistrationPage(WebDriver driver){
        super(driver);
    }

    public WebElement getRegistrationForm(){
        return registrationForm;
    }
    public void clickCreateAccountButton(){
        createAccountButton.click();
    }
    public WebElement getWarningMissedEmail(){
        return warningMissedEmail;
    }
    public WebElement getWarningMissedName(){
        return warningMissedName;
    }
    public WebElement getWarningMissedPassword(){
        return warningMissedPassword;
    }
}
