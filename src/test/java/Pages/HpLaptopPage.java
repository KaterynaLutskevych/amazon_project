package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HpLaptopPage extends BasePage {

    @FindBy(xpath="//input[@id='add-to-cart-button']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//span[contains(@class,'nav-logo-base')]")
    private WebElement logo;

    @FindBy(xpath="//span[@id='priceblock_ourprice']")
    private String priceOfItemBeforeAddedToCart;

    @FindBy(xpath="//span[@id='attach-accessory-cart-subtotal']")
    private String priceOfItemAfterAddedToCart;


    public HpLaptopPage (WebDriver driver){
        super(driver);
    }

    public void clickAddToCartButton(){
        addToCartButton.click();
    }
    public WebElement getAddToCartButton(){
        return addToCartButton;
    }
    public void clickLogo(){
        logo.click();
    }
    public String getPriceOfItemBeforeAddedToCart(){
        return priceOfItemBeforeAddedToCart;
    }
    public String getPriceOfItemAfterAddedToCart(){
        return priceOfItemAfterAddedToCart;
    }

}
